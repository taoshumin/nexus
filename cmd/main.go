package main

import (
	"github.com/sirupsen/logrus"
	nexusrm "github.com/sonatype-nexus-community/gonexus/rm"
	cli "github.com/urfave/cli/v2"
	"gitlab.gridsum.com/nexus/client"
	"os"
)

var (
	log                *logrus.Logger
	repository         string
	format             string
	keyword            string
	id                 string
	upload_package_dir string
	upload_nexus_dir   string
	upload_repo        string
)

func main() {
	log = NewLog()
	if err := NewApp(log).Run(os.Args); err != nil {
		log.WithField("app", "run").Panic(err)
	}
}

func NewLog() *logrus.Logger {
	log := logrus.New()
	log.Out = os.Stdout
	log.Formatter = &logrus.TextFormatter{}
	return log
}

func NewApp(log *logrus.Logger) *cli.App {
	app := cli.NewApp()
	app.Name = "nexus"
	app.Usage = "upload nexus package"
	app.Version = "1.0"

	app.Flags = []cli.Flag{
		&cli.StringFlag{
			Name:        "repository",
			Aliases:     []string{"r"},
			Value:       "raw",
			Usage:       "Select the repository type to query;default raw",
			Destination: &repository,
		},
		&cli.StringFlag{
			Name:        "format",
			Aliases:     []string{"f"},
			Value:       "raw",
			Usage:       "Select the format type to query;default raw",
			Destination: &format,
		},
		&cli.StringFlag{
			Name:        "keyword",
			Aliases:     []string{"k"},
			Value:       "iiotdepartment",
			Usage:       "Select the keyword to query;default iiotdepartment",
			Destination: &keyword,
		},
		&cli.StringFlag{
			Name:        "id",
			Aliases:     []string{"i"},
			Usage:       "choose delete's id;default empty",
			Destination: &id,
		},
		&cli.StringFlag{
			Name:        "repo",
			Aliases:     []string{"p"},
			Value:       "raw",
			Usage:       "choose upload's repo ;default raw",
			Destination: &upload_repo,
		},
		&cli.StringFlag{
			Name:        "pkdir",
			Aliases:     []string{"pd"},
			Usage:       "choose upload's package dir;default empty",
			Destination: &upload_package_dir,
		},
		&cli.StringFlag{
			Name:        "nxdir",
			Aliases:     []string{"nd"},
			Usage:       "choose upload's package dir;default empty",
			Destination: &upload_nexus_dir,
		},
	}
	app.Commands = []*cli.Command{
		{
			Name:    "get",
			Aliases: []string{"g"},
			Usage:   "GET Nexus's rm component list",
			Action: action(func(rm nexusrm.RM, context *cli.Context) error {
				resp, err := client.NewClient(rm).Search(client.GET{repository, format, keyword,})
				if err != nil {
					return err
				}
				log.WithField("GET", "Search").Info(resp)
				return nil
			}),
		},
		{
			Name:    "delete",
			Aliases: []string{"d"},
			Usage:   "DELETE Nexus's rm component",
			Action: action(func(rm nexusrm.RM, context *cli.Context) error {
				err := client.NewClient(rm).Delete(client.DELETE{id})
				if err != nil {
					return err
				}
				log.WithField("DELETE", id).Info("delete component sucess")
				return nil
			}),
		},
		{
			Name:    "upload",
			Aliases: []string{"u"},
			Usage:   "UPLOAD Nexus's rm component",
			Action: action(func(rm nexusrm.RM, context *cli.Context) error {
				assets,err := client.NewClient(rm).UploadRow(client.UPLOAD{UploadPackageDir: upload_package_dir,UploadNexusDir: upload_nexus_dir,UploadRepo: upload_repo})
				if err != nil {
					return err
				}
				log.WithField("Upload", upload_package_dir).Info(assets)
				return nil
			}),
		},
	}
	return app
}

func action(f func(nexusrm.RM, *cli.Context) error) cli.ActionFunc {
	return func(context *cli.Context) error {
		rm, err := nexusrm.New("http://10.200.64.38:8081", "admin", "admin123")
		if err != nil {
			panic(err)
		}
		return f(rm, context)
	}
}
