package client

import (
	nexusrm "github.com/sonatype-nexus-community/gonexus/rm"
	"reflect"
	"testing"
)

func TestNewClient(t *testing.T) {
	type args struct {
		rm nexusrm.RM
	}
	tests := []struct {
		name string
		args args
		want *client
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := NewClient(tt.args.rm); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("NewClient() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_client_Delete(t *testing.T) {
	type fields struct {
		rm nexusrm.RM
	}
	type args struct {
		dt DELETE
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			c := &client{
				rm: tt.fields.rm,
			}
			if err := c.Delete(tt.args.dt); (err != nil) != tt.wantErr {
				t.Errorf("Delete() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func Test_client_Load(t *testing.T) {
	type fields struct {
		rm nexusrm.RM
	}
	type args struct {
		fileDir string
	}
	tests := []struct {
		name       string
		fields     fields
		args       args
		wantAssets []nexusrm.UploadAssetRaw
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			c := &client{
				rm: tt.fields.rm,
			}
			if gotAssets := c.Load(tt.args.fileDir); !reflect.DeepEqual(gotAssets, tt.wantAssets) {
				t.Errorf("Load() = %v, want %v", gotAssets, tt.wantAssets)
			}
		})
	}
}

func Test_client_Pretty(t *testing.T) {
	type fields struct {
		rm nexusrm.RM
	}
	type args struct {
		body interface{}
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   []byte
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			c := &client{
				rm: tt.fields.rm,
			}
			if got := c.Pretty(tt.args.body); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Pretty() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_client_Search(t *testing.T) {
	type fields struct {
		rm nexusrm.RM
	}
	type args struct {
		gt GET
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    interface{}
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			c := &client{
				rm: tt.fields.rm,
			}
			got, err := c.Search(tt.args.gt)
			if (err != nil) != tt.wantErr {
				t.Errorf("Search() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Search() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_client_UploadRow(t *testing.T) {
	type fields struct {
		rm nexusrm.RM
	}
	type args struct {
		up UPLOAD
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    []string
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			c := &client{
				rm: tt.fields.rm,
			}
			got, err := c.UploadRow(tt.args.up)
			if (err != nil) != tt.wantErr {
				t.Errorf("UploadRow() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("UploadRow() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_contains(t *testing.T) {
	type args struct {
		exts   []string
		target string
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := contains(tt.args.exts, tt.args.target); got != tt.want {
				t.Errorf("contains() = %v, want %v", got, tt.want)
			}
		})
	}
}