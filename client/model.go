package client

type GET struct {
	Repository string
	Format     string
	Keyword    string
}

type DELETE struct {
	ID string
}

type UPLOAD struct {
	UploadPackageDir string
	UploadNexusDir   string
	UploadRepo       string
}
