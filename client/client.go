package client

/*
	Repository	raw			Group	/iiotdepartment/cpsdb/v1.0.0
	Format	raw				Name	iiotdepartment/cpsdb/v1.0.0/cpsdb-v1.0.0.linux.arm64.rpm
*/

import (
	"bytes"
	"encoding/json"
	"errors"
	nexusrm "github.com/sonatype-nexus-community/gonexus/rm"
	"io/ioutil"
	"os"
	"runtime"
	"strings"
)

var support = []string{"deb", "rpm"}

type Support interface {
	Pretty(body interface{}) []byte
	Load(fileDir string) (assets []nexusrm.UploadAssetRaw)
}

type client struct {
	rm nexusrm.RM
}

func NewClient(rm nexusrm.RM) *client {
	return &client{rm: rm}
}

// Search is use to search by keywords
// sch := nexusrm.NewSearchQueryBuilder().Q(gt.Keyword)
func (c *client) Search(gt GET) (interface{}, error) {
	sch := nexusrm.NewSearchQueryBuilder().Repository(gt.Repository).Format(gt.Format).Q(gt.Keyword)
	resp, err := nexusrm.SearchComponents(c.rm, sch)
	if err != nil {
		return nil, err
	}
	return string(c.Pretty(resp)), nil
}

// Upload is use to upload rpm package, pkDir is the folder to upload,remotDir is to the file saved by the server
func (c *client) UploadRow(up UPLOAD) ([]string,error) {
	assets := c.Load(up.UploadPackageDir)
	if len(assets) == 0 {
		return nil,errors.New("no rpm & deb package file in the file")
	}
	upload := &nexusrm.UploadComponentRaw{
		Directory: up.UploadNexusDir,
		Assets:    assets,
	}
	if err := nexusrm.UploadComponent(c.rm, up.UploadRepo, upload); err != nil {
		return nil,err
	}
	var asset []string
	for _, up := range upload.Assets {
		asset = append(asset, up.Filename)
	}
	return asset,nil
}

// Delete is use to delete component by id
func (c *client) Delete(dt DELETE) error {
	if err := nexusrm.DeleteComponentByID(c.rm, dt.ID); err != nil {
		return err
	}
	return nil
}

// Pretty is use to print pretty json
func (c *client) Pretty(body interface{}) []byte {
	b, err := json.Marshal(body)
	if err != nil {
		return nil
	}
	var prettyJSON bytes.Buffer
	err = json.Indent(&prettyJSON, b, "", "\t")
	if err != nil {
		return nil
	}
	return prettyJSON.Bytes()
}

// Load is use to deb and rpm
func (c *client) Load(fileDir string) (assets []nexusrm.UploadAssetRaw) {
	filesInfo, err := ioutil.ReadDir(fileDir)
	if err != nil {
		return
	}
	for _, file := range filesInfo {
		ext := file.Name()[strings.LastIndex(file.Name(), ".")+1:]
		contains := contains(support[0:], ext)
		if contains {
			globpath, _ := os.Getwd()
			if runtime.GOOS == "window"{
				 globpath = strings.Join([]string{globpath,file.Name()},"\\")
			}else {
				globpath = strings.Join([]string{fileDir,file.Name()},"/")
			}
			if read, err := ioutil.ReadFile(globpath); err == nil {
				reader := bytes.NewReader(read)
				assets = append(assets, nexusrm.UploadAssetRaw{
					File:     reader,
					Filename: file.Name(),
				})
			}
		}
	}
	return
}

// contains is use to confirm whether contains deb & rpm package
func contains(exts []string, target string) bool {
	contains := false
	for _, ext := range exts {
		if ext == target {
			contains = true
			break
		}
	}
	return contains
}
