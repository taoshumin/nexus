module gitlab.gridsum.com/nexus

go 1.16

require (
	github.com/sirupsen/logrus v1.8.1
	github.com/sonatype-nexus-community/gonexus v0.59.0
	github.com/urfave/cli/v2 v2.3.0
	golang.org/x/sys v0.0.0-20201119102817-f84b799fce68 // indirect
)
